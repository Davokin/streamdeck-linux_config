#!/bin/bash

# Überprüfe, ob playerctl installiert ist
if ! command -v playerctl &> /dev/null; then
    echo "playerctl ist nicht installiert. Bitte installieren Sie es zuerst."
    exit 1
fi

# Funktion zum Ermitteln des aktiven Players
get_active_player() {
    playerctl metadata --format '{{lc(status)}} {{lc(playerName)}}' 2>/dev/null
}

# Funktion zum Ermitteln des Shuffle-Status
get_shuffle_status() {
    playerctl -a shuffle
}

# Hauptfunktion zum Wechseln zwischen Play und Pause und Shuffle
toggle_play_pause() {
    active_player=$(get_active_player)
    status=$(echo "$active_player" | awk '{print $1}')
    shuffle_status=$(get_shuffle_status)

    case $status in
        "playing")
            playerctl -p "$active_player" pause
            ;;
        "paused")
            playerctl -p "$active_player" play
            ;;
        *)
            echo "Unbekannter Status: $status"
            ;;
    esac

    # Wechseln zwischen Shuffle ein und Shuffle aus
    if [ "$shuffle_status" = "Shuffle: off" ]; then
        playerctl -a shuffle
        echo "Shuffle aktiviert."
    else
        playerctl -a shuffle off
        echo "Shuffle deaktiviert."
    fi
}

# Endlosschleife zum wiederholten Aufruf der Hauptfunktion
while true; do
    toggle_play_pause
    sleep 1  # Ändern Sie die Wartezeit nach Bedarf
done
