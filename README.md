# StreamDeck Linux Config

![Example configuration](https://codeberg.org/Davokin/streamdeck-linux_config/raw/branch/master/audio-page.png)

This repository contains personalized commands and configurations for the StreamDeck Linux UI. Customize your StreamDeck experience with tailored commands, shortcuts, and UI configurations to enhance your workflow on Linux.

### ![pactl-change.sh](https://codeberg.org/Davokin/streamdeck-linux_config/src/branch/master/pactl-change.sh)

This Bash script dynamically switches between two audio output devices based on the current default input device. The defined input and output devices are compared, and the script changes the default output device accordingly, providing a simple and automated way to manage audio configurations.

### ![pp-audio.sh](https://codeberg.org/Davokin/streamdeck-linux_config/src/branch/master/pp-audio.sh)

This Bash script checks for the presence of playerctl, a command-line utility for controlling media players, and then defines functions to get the active player, retrieve shuffle status, and toggle play/pause and shuffle. The script runs in an infinite loop, continuously toggling play/pause and shuffle state for the active media player.

## Features

- **Personalized Commands:** Tailor the StreamDeck to your specific needs with a set of personalized commands designed to optimize your daily tasks.
  
- **Linux UI Configurations:** Fine-tune the StreamDeck Linux UI to match your preferences. Create a workspace that aligns perfectly with your workflow, making navigation and execution seamless.

- **Developer-Friendly:** Easily integrate new commands and configurations, allowing developers to contribute and collaborate on enhancing the functionality of the StreamDeck for Linux.

## Getting Started

1. **Clone this Repository:**
   ```bash
   git clone https://codeberg.org/Davokin/streamdeck-linux_config.git
   ```

2. **Import Configurations:**
   - Open StreamDeck Linux UI.
   - Import configurations from the cloned repository.

3. **Customize:**
   - Add your own commands.
   - Modify configurations to suit your workflow.

## Contributing

If you have suggestions, improvements, or new configurations to share, feel free to contribute! Follow these steps:

1. Fork the repository.
2. Create a new branch: `git checkout -b feature/new-feature`.
3. Make your changes and commit: `git commit -am 'Add new feature'`.
4. Push to the branch: `git push origin feature/new-feature`.
5. Submit a pull request.

## Acknowledgments

- Official ![streamdeck-linux-gui](https://github.com/streamdeck-linux-gui/streamdeck-linux-gui) project
