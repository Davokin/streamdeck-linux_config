#!/bin/bash

# Definiere die Namen der beiden Audioausgabegeräte
eingabe_geraet="alsa_output.usb-Astro_Gaming_Astro_A50-00.stereo-game"
ausgabe_geraet_1="alsa_output.pci-0000_0a_00.4.analog-stereo"
ausgabe_geraet_2="alsa_output.usb-Astro_Gaming_Astro_A50-00.stereo-game"

# Überprüfe das aktuelle Eingabegerät
aktuelles_geraet=$(pactl info | grep "Default Sink" | awk '{print $3}')

# Vergleiche das aktuelle Eingabegerät mit den definierten Geräten
if [ "$aktuelles_geraet" == "$eingabe_geraet" ]; then
    # Wechsle zu Ausgabegerät 1, wenn das aktuelle Eingabegerät gleich ist
    pactl set-default-sink "$ausgabe_geraet_1"
    echo "Wechsel zu $ausgabe_geraet_1"
else
    # Wechsle zu Ausgabegerät 2, wenn das aktuelle Eingabegerät unterschiedlich ist
    pactl set-default-sink "$ausgabe_geraet_2"
    echo "Wechsel zu $ausgabe_geraet_2"
fi
